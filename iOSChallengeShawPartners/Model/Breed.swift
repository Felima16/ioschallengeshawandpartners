//
//  Model.swift
//  iOSChallengeShawPartners
//
//  Created by Fernanda de Lima on 05/09/18.
//  Copyright © 2018 Fernanda de Lima. All rights reserved.
//

import Foundation
import RealmSwift

class Breed: Object{
    @objc dynamic var name:String = ""
}


class Image: Object{
    @objc dynamic var url:String = ""
    @objc dynamic var breed:String = ""
}
