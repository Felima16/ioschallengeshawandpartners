//
//  LocalManager.swift
//  Notepic
//
//  Created by Fernanda de Lima on 15/05/2018.
//  Copyright © 2018 FeLima. All rights reserved.
//

import Foundation
import RealmSwift

let localMan = LocalManager.instance

class LocalManager: NSObject {
    
    //Singleton struct
    static let instance = LocalManager()
    
    var breesRemotes:[String]=[]
    var imagesBreedsRemote:[String]=[]
    
    let realm = try! Realm()
    lazy var breeds: Results<Breed> = { self.realm.objects(Breed.self) }()
    
    //add new object on realm
    func addBreed(breed: Breed){
        try! realm.write {
            self.realm.add(breed)
        }
        breeds = realm.objects(Breed.self)
    }
    
    //delete object from
    func deleteBreed(index: Int){
        try! realm.write {
            realm.delete(breeds[index])
        }
        breeds = realm.objects(Breed.self)
    }
    
    func isExistBreed(breed:String) -> Bool{
        let predicate = NSPredicate(format: "name = %@", breed)
        let breedRealm = realm.objects(Breed.self).filter(predicate)
        
        if breedRealm.count > 0{
            return true
        }else{
            return false
        }
    
    }

}
