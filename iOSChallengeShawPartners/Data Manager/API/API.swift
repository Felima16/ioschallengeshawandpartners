
//
//  API.swift
//  ChallengeWaferMessenger
//
//  Created by Fernanda de Lima on 21/08/2018.
//  Copyright © 2018 FeLima. All rights reserved.
//

import Foundation
import UIKit

enum Endpoint {
    
    case breeds
    case images(String)
    
    func pathEndpoint() ->String {
        switch self {
        case .breeds:
            return "/api/breeds"
        case .images(let name):
            return "/api/\(name)/images"
        }
    }
    
}


class API{
    
    static let baseUrl = "https://hidden-crag-71735.herokuapp.com"
    
    class func get(breed:String?,
        success:@escaping (_ item: [String]) -> Void,
        fail:@escaping (_ error: Error) -> Void) -> Void
    {
        let url = breed != nil ? "\(baseUrl)\(Endpoint.images(breed!).pathEndpoint())" : "\(baseUrl)\(Endpoint.breeds.pathEndpoint())"
        
        //create request to API
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        //create session to connection
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            do {
                
                //verify response
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode == 200{ //it's ok
                        //verify if have response data
                        if let data = data{
                            if let jsonArray = try JSONSerialization.jsonObject(with: data, options: []) as? [String] {
                                success(jsonArray)
                            }
                            
                        }
                    }
                }
                
            } catch {
                fail(error)
            }
            
        })
        
        task.resume()
    }
}
