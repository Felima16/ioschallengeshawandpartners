//
//  ImageCollectionViewCell.swift
//  iOSChallengeShawPartners
//
//  Created by Fernanda de Lima on 06/09/18.
//  Copyright © 2018 Fernanda de Lima. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
}
