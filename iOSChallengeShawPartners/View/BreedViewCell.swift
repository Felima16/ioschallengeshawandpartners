//
//  BreedViewCell.swift
//  iOSChallengeShawPartners
//
//  Created by Fernanda de Lima on 06/09/18.
//  Copyright © 2018 Fernanda de Lima. All rights reserved.
//

import UIKit

class BreedViewCell: UITableViewCell {
    
    //IBOutlet
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    //IBAction
    @IBAction func favoriteActionButton(_ sender: Any) {
        if localMan.isExistBreed(breed: nameLabel.text!){
            let predicate = NSPredicate(format: "name = %@", nameLabel.text!)
            let index = localMan.breeds.index(matching: predicate)
            localMan.deleteBreed(index: index!)
            favoriteButton.setImage(#imageLiteral(resourceName: "starIconA"), for: .normal)
        }else{
            let breedRealm = Breed()
            breedRealm.name = nameLabel.text!
            localMan.addBreed(breed: breedRealm)
            favoriteButton.setImage(#imageLiteral(resourceName: "starIconB"), for: .normal)
        }
    }

}
