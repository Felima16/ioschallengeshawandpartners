//
//  FavoritesViewController.swift
//  iOSChallengeShawPartners
//
//  Created by Fernanda de Lima on 06/09/18.
//  Copyright © 2018 Fernanda de Lima. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController {
    
    @IBOutlet weak var favoritesCollectionView: UICollectionView!
    
    var listFav:[[String]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initList()
    }
    
    private func initList(){
        for breed in localMan.breeds{
            API.get(breed: breed.name, success: { (images) in
                var list:[String] = []
                
                for i in 0..<5{
                    list.append(images[i])
                }
                self.listFav.append(list)
                
                DispatchQueue.main.async {
                    self.favoritesCollectionView.reloadData()
                }
                
            }) { (error) in
                print(error.localizedDescription)
            }
        }
    }

}

extension FavoritesViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listFav[section].count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return listFav.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionElementKindSectionHeader:
            let reusableview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "breedNameView", for: indexPath) as! BreedNameView
            
//            reusableview.frame = CGRect(0 , 0, self.view.frame.width, headerHight)
            //do other header related calls or settups
            
            reusableview.titleLabel.text = localMan.breeds[indexPath.section].name
            return reusableview
            
            
        default:  fatalError("Unexpected element kind")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageIdentifier", for: indexPath) as! ImageCollectionViewCell
        if let url = URL(string: listFav[indexPath.section][indexPath.row]){
            let data = try? Data(contentsOf:url)
            cell.imageView?.image = UIImage(data: data!)
        }
        return cell
    }
    
    

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var thumbnailSize:CGSize = CGSize.zero
        let itemSpacing:CGFloat = 10
        
        //print("Called collectionView collectionViewLayout sizeForItemAt")
        // Calculate thumbnail size based on device
        if traitCollection.userInterfaceIdiom == .pad {
            //print("iPad")
            if UIDevice.current.orientation.isLandscape {
                thumbnailSize.width = floor((collectionView.frame.size.width - itemSpacing * 5) / 4)
            } else {
                thumbnailSize.width = floor((collectionView.frame.size.width - itemSpacing * 4) / 3)
            }
        } else if traitCollection.userInterfaceIdiom == .phone {
            //print("iPhone")
            thumbnailSize.width = floor((collectionView.frame.size.width - itemSpacing * 3) / 3)
        }
        thumbnailSize.height = thumbnailSize.width
        return thumbnailSize
    }
    
    
    
}
