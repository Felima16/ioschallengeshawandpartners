//
//  ImagesBreedViewController.swift
//  iOSChallengeShawPartners
//
//  Created by Fernanda de Lima on 06/09/18.
//  Copyright © 2018 Fernanda de Lima. All rights reserved.
//

import UIKit

class ImagesBreedViewController: UIViewController {

    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    var breed:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImages()
        self.navigationController?.title = breed
    }
    
    private func loadImages(){
        API.get(breed: breed, success: { (images) in
            print(images)
            localMan.imagesBreedsRemote = images
            
            DispatchQueue.main.async {
                self.imageCollectionView.reloadData()
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}


extension ImagesBreedViewController : UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return localMan.imagesBreedsRemote.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageIdentifier", for: indexPath) as! ImageCollectionViewCell
        if let url = URL(string: localMan.imagesBreedsRemote[indexPath.row]){
            let data = try? Data(contentsOf:url)
            cell.imageView.image = UIImage(data: data!)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var thumbnailSize:CGSize = CGSize.zero
        let itemSpacing:CGFloat = 10
        
        //print("Called collectionView collectionViewLayout sizeForItemAt")
        // Calculate thumbnail size based on device
        if traitCollection.userInterfaceIdiom == .pad {
            //print("iPad")
            if UIDevice.current.orientation.isLandscape {
                thumbnailSize.width = floor((collectionView.frame.size.width - itemSpacing * 5) / 4)
            } else {
                thumbnailSize.width = floor((collectionView.frame.size.width - itemSpacing * 4) / 3)
            }
        } else if traitCollection.userInterfaceIdiom == .phone {
            //print("iPhone")
            thumbnailSize.width = floor((collectionView.frame.size.width - itemSpacing * 3) / 3)
        }
        thumbnailSize.height = thumbnailSize.width
        return thumbnailSize
    }
    
    
}
