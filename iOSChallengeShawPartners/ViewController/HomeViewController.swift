//
//  ViewController.swift
//  iOSChallengeShawPartners
//
//  Created by Fernanda de Lima on 05/09/18.
//  Copyright © 2018 Fernanda de Lima. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var breedsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadBreeds()
    }
    
    private func loadBreeds(){
        API.get(breed: nil, success: { (breeds) in
            print(breeds)
            localMan.breesRemotes = breeds
            
            DispatchQueue.main.async {
                self.breedsTableView.reloadData()
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }


}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return localMan.breesRemotes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "breedIdentifier") as! BreedViewCell
        cell.nameLabel.text = localMan.breesRemotes[indexPath.row]
        let imgFav = localMan.isExistBreed(breed: localMan.breesRemotes[indexPath.row]) ? #imageLiteral(resourceName: "starIconB") : #imageLiteral(resourceName: "starIconA")
        cell.favoriteButton.setImage(imgFav, for: .normal)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let imagesBreedViemController = storyboard?.instantiateViewController(withIdentifier: "imagesBreedViewController") as! ImagesBreedViewController
        imagesBreedViemController.breed = localMan.breesRemotes[indexPath.row]
        self.navigationController?.pushViewController(imagesBreedViemController, animated: true)
    }
    
    
}
